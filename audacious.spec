Name:           audacious
Version:        4.2
Release:        1%{?dist}
Summary:        Latest Audacious Stable

License:        BSD
URL:            https://www.audacious-media-player.org/download
Source0:        https://distfiles.audacious-media-player.org/%{name}-%{version}.tar.bz2 

BuildRequires: libappstream-glib
BuildRequires: gcc-c++
BuildRequires: gettext
%{?with_gtk:BuildRequires: pkgconfig(gtk+-2.0)}
BuildRequires: pkgconfig(glib-2.0)
BuildRequires: desktop-file-utils
BuildRequires: make
BuildRequires: qt5-qtbase-devel
BuildRequires: pkgconfig(Qt5Core)
BuildRequires: pkgconfig(Qt5Gui)
BuildRequires: pkgconfig(Qt5Widgets)
BuildRequires: glib2-devel
BuildRequires: gtk2-devel

%description
Audacious Media Player

%prep
%autosetup


%build
%configure
%make_build


%install
%make_install INSTALL="install -p"
find ${RPM_BUILD_ROOT} -type f -name "*.la" -exec rm -f {} ';'

%find_lang %{name}
	
desktop-file-install  \
    --dir ${RPM_BUILD_ROOT}%{_datadir}/applications  \
    ${RPM_BUILD_ROOT}%{_datadir}/applications/audacious.desktop
	
install -D -m0644 contrib/%{name}.appdata.xml ${RPM_BUILD_ROOT}%{_datadir}/appdata/%{name}.appdata.xml
appstream-util validate-relax --nonet ${RPM_BUILD_ROOT}%{_datadir}/appdata/%{name}.appdata.xml

%files -f %{name}.lang
%doc AUTHORS
%license COPYING
%{_bindir}/audacious
%{_bindir}/audtool
%{_datadir}/audacious/
%{_mandir}/man[^3]/*
%{_datadir}/applications/*.desktop
%{_datadir}/icons/hicolor/*/apps/%{name}*.*
%{_datadir}/appdata/%{name}.appdata.xml


%{_libdir}/*.so.*
%{_includedir}/audacious/
%{_includedir}/libaudcore/
%{_includedir}/libaudqt/
%{_includedir}/libaudgui/
%{_libdir}/*.so
%{_libdir}/pkgconfig/*.pc



%changelog
* Wed Jul 13 2022 clizby
- 
